// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_trigger.h"
/* ========================================================================== */
// DAQmxDisableStartTrig
// [status] = DAQ_DisableStartTrig( taskHandle )
DAQ_TRIGGER_1( DAQ_DisableStartTrig, DAQmxDisableStartTrig );
/* ========================================================================== */
// DAQmxDisableRefTrig
// [status] = DAQ_DisableRefTrig( taskHandle )
DAQ_TRIGGER_1( DAQ_DisableRefTrig, DAQmxDisableRefTrig );
/* ========================================================================== */
