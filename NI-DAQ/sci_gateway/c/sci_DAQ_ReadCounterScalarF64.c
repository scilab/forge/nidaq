// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_read.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxReadCounterScalarF64
// [value, status] = DAQ_ReadCounterScalarF64( taskHandle, timeout )
int sci_DAQ_ReadCounterScalarF64( char* fname )
{
  TaskHandle taskHandle = 0;
  float64 timeout = 0.0;
  float64 *value = NULL;
  int32 status = 0;

  int m = 0, n = 0, l = 0;

  CheckRhs( 2, 2 );
  CheckLhs( 1, 2 );

  GetRhsTaskHandle( 1, taskHandle,  m, n, l );
  GetRhsFloat64( 2, timeout, m, n, l );

  m = 1;
  n = 1;
  CreateVar( 3, MATRIX_OF_DOUBLE_DATATYPE ,&m ,&n , &l);
  value = stk( l );

  status = DAQmxReadCounterScalarF64( taskHandle,
                                          timeout,
                                          value,
                                          ( bool32* )NULL );

  CreateInt32( 4, status, m, n, l );

  LhsVar( 1 ) = 3;
  LhsVar( 2 ) = 4;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */
