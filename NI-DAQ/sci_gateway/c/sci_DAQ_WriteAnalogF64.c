// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_write.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxWriteAnalogF64
// [sampsPerChanWritten, status] =
//  DAQ_WriteAnalogF64( taskHandle, numSampsPerChan, timeout,
//                      dataLayout, writeArray )
int sci_DAQ_WriteAnalogF64( char* fname )
{
  TaskHandle taskHandle = 0;
  int32 numSampsPerChan = 0;
  float64 timeout = 0.0;
  bool32 dataLayout = 0;
  float64 *writeArray = NULL;
  int32 sampsPerChanWritten = 0;
  int32 status = 0;

  int m = 0, n = 0, l = 0;

  CheckRhs( 5, 5 );
  CheckLhs( 1, 2 );

  GetRhsTaskHandle( 1, taskHandle,  m, n, l );
  GetRhsInt32(2, numSampsPerChan, m, n, l );
  GetRhsFloat64( 3, timeout, m, n, l );
  GetRhsInt32( 4, dataLayout, m, n, l );

//  FIXME: GetRhsArrayFloat64?
    GetRhsVar( 5, MATRIX_OF_DOUBLE_DATATYPE, &m, &n, &l);
  writeArray = stk( l );
  CheckVector( 5, m, n );

  status = DAQmxWriteAnalogF64( taskHandle,  numSampsPerChan,
                                    FALSE , timeout, dataLayout,
                                    writeArray, &sampsPerChanWritten,
                                    ( bool32* )NULL );

  CreateInt32( 6, sampsPerChanWritten, m, n, l );
  CreateInt32( 7, status, m, n, l );

  LhsVar( 1 ) = 6;
  LhsVar( 2 ) = 7;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */
