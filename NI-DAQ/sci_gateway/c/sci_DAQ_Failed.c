// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011  Allan CORNET - DIGITEO
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq.h"
#include "MALLOC.h" // MALLOC & FREE
#include "Scierror.h"
/* ========================================================================== */
// DAQmxFailed
// [ hasFailed ] = DAQ_Failed( status )
int sci_DAQ_Failed( char* fname )
{
  int32 status = 0;
  bool32 hasFailed = 0;

  int m = 0, n = 0, l = 0;

  CheckRhs( 1, 1 );
  CheckLhs( 1, 1 );

  GetRhsInt32( 1, status, m, n, l );

  hasFailed = DAQmxFailed( status );

  CreateBool32( 2, hasFailed, m, n, l );

  LhsVar(1) = 2;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */
