// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_read.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxReadCounterU32
// [readArray, sampsPerChanRead, status] =
// DAQ_ReadCounterU32( taskHandle, numSampsPerChan, timeout,
//                     arraySizeInSamps )
int sci_DAQ_ReadCounterU32( char* fname )
{
  TaskHandle taskHandle = 0;
  int32 numSampsPerChan = 0;
  float64 timeout = 0.0;
  uInt32 *readArray = NULL;
  uInt32 arraySizeInSamps = 0;
  int32 sampsPerChanRead = 0;
  int32 status = 0;

  int m = 0, n = 0, l = 0;

  CheckRhs( 4, 4 );
  CheckLhs( 1, 3 );

  GetRhsTaskHandle( 1, taskHandle,  m, n, l );
  GetRhsInt32(2, numSampsPerChan, m, n, l );
  GetRhsFloat64( 3, timeout, m, n, l );
  GetRhsUInt32( 4, arraySizeInSamps, m, n, l );

  m = 1;
  n = arraySizeInSamps;
  l = I_UINT32;
  CreateVar( 5, MATRIX_OF_VARIABLE_SIZE_INTEGER_DATATYPE ,&m ,&n , &l);
  readArray = IC_UINT32( istk( l ) );

  status = DAQmxReadCounterU32( taskHandle, numSampsPerChan,
                                    timeout,
                                    readArray, arraySizeInSamps,
                                    &sampsPerChanRead,
                                    ( bool32* )NULL );

  CreateInt32( 6, sampsPerChanRead, m, n, l );
  CreateInt32( 7, status, m, n, l );

  LhsVar( 1 ) = 5;
  LhsVar( 2 ) = 6;
  LhsVar( 3 ) = 7;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */
