// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_write.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxWriteDigitalU8
// [sampsPerChanWritten, status] =
//  DAQ_WriteDigitalU8( taskHandle, numSampsPerChan, autoStart,
//                      timeout, dataLayout, writeArray )
DAQ_WRITE_1( DAQ_WriteDigitalU8, UCHAR, DAQmxWriteDigitalU8, uInt8 );
/* ========================================================================== */
// DAQmxWriteDigitalU32
// [sampsPerChanWritten, status] =
//  DAQ_WriteDigitalU32( taskHandle, numSampsPerChan, autoStart,
//                       timeout, dataLayout, writeArray )
DAQ_WRITE_1( DAQ_WriteDigitalU32, UINT32,
             DAQmxWriteDigitalU32, uInt32 );
/* ========================================================================== */
