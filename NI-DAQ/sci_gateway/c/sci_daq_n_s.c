// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq.h"
#include "sciprint.h"
/* ========================================================================== */
// not-equal-relation ( taskHandle ~= value )
int sci_daq_n_s( char* fname )
{
  TaskHandle taskHandle = 0;
  int value = 0;

  int m = 0, n = 0, l = 0;

  CheckRhs( 2, 2 );
  CheckLhs( 1, 1 );

  GetRhsTaskHandle( 1, taskHandle, m, n, l );

  GetRhsVar( 2, MATRIX_OF_INTEGER_DATATYPE, &m, &n, &l );
  value = *istk( l );

  m = 1; n = 1;
  CreateVar( 3, MATRIX_OF_BOOLEAN_DATATYPE, &m, &n, &l );
  *istk( l ) = ( taskHandle != value );

  LhsVar(1) = 3;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */
