<?xml version="1.0" encoding="ISO-8859-1"?>
<refentry version="5.0-subset Scilab" xml:id="DAQ_Error_Handling"
          xml:lang="en" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>07-Oct-2011</pubdate>
  </info>

  <refnamediv>
    <refname>DAQ_Error_Handling</refname>

    <refpurpose>DAQ_Success</refpurpose>
    <refpurpose>DAQ_Failed</refpurpose>
    <refpurpose>DAQ_ErrChk</refpurpose>
    <refpurpose>DAQ_GetExtendedErrorInfo</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>[ value ] = DAQ_Success ()
[ failed ] = DAQ_Failed ( status )
[] = DAQ_ErrChk ( taskHandle, status )
[ errorString ] = DAQ_GetExtendedErrorInfo ()</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>value</term>

        <listitem>
          <para>integer</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>failed</term>

        <listitem>
          <para>boolean</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>errorString</term>

        <listitem>
          <para>string</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">DAQ_Success</emphasis> returns a constant
        value (0), which is returned by NI-DAQmx functions to indicate
        successful operation.</para>

        <para><emphasis role="bold">DAQ_Failed</emphasis> can be used to check
        the <emphasis role="bold">status</emphasis> of an operation. It
        returns <emphasis role="bold">T</emphasis> if the operation failed and
        <emphasis role="bold">F</emphasis> otherwise.</para>

        <para><emphasis role="bold">DAQ_ErrChk</emphasis> checks the <emphasis
        role="bold">status</emphasis> of an operation and in case of a failure
        the associated task specified by <emphasis
        role="bold">taskHandle</emphasis> is stopped and cleared and an
        apropriate error message is issued (see implementation of DAQ_ErrChk
        in Examples section below).</para>

        <para><emphasis role="bold">DAQ_GetExtendedErrorInfo</emphasis>
        provides just an interface to <emphasis
        role="bold">DAQmxGetExtendedErrorInfo</emphasis>.</para>

        <para>For more detailed information about the interfaced functions,
        please refer to <emphasis>NI-DAQmx 9.x C Function Reference
        Help</emphasis>.</para>
      </listitem>
    </itemizedlist>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">   // implementation of DAQ_ErrChk
   function [] = DAQ_ErrChk( taskHandle, status )
     if DAQ_Failed( status ) then
       errorString = DAQ_GetExtendedErrorInfo();
         if taskHandle ~= 0 then
           DAQ_StopTask( taskHandle );
           DAQ_ClearTask( taskHandle );
         end
      error( 'DAQmx Error: ' + string(status) + ': '...
                                 + errorString, 10000 );
     end
   endfunction
</programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="DAQ_Constants">DAQ_Constants</link></member>

      <member><link
      linkend="DAQ_Task_Configuration_and_Control">DAQ_Task_Configuration_and_Control</link></member>

      <member><link
      linkend="DAQ_Channel_Configuration_and_Creation">DAQ_Channel_Configuration_and_Creation</link></member>

      <member><link linkend="DAQ_Timing">DAQ_Timing</link></member>

      <member><link linkend="DAQ_Triggering">DAQ_Triggering</link></member>

      <member><link
      linkend="DAQ_Read_Functions">DAQ_Read_Functions</link></member>

      <member><link
      linkend="DAQ_Write_Functions">DAQ_Write_Functions</link></member>

    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <variablelist>
      <varlistentry>
        <term>Dirk Reusch</term>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>NI-DAQmx 9.x C Function Reference Help</para>
  </refsection>
</refentry>
