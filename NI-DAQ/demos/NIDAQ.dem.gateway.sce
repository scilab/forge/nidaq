// Copyright (C) 2011 - DIGITEO - Allan CORNET


function subdemolist = demo_gateway()
  demopath = get_absolute_file_path("NIDAQ.dem.gateway.sce");

  subdemolist = ["Acquires one sample from an analog input channel",           "acquire1Scan.sce"; ..
                 "Acquires a finite amount of data from analog input channel", "acquireNScans.sce"; ..
                 "Measure pulse width on a counter input channel",             "pulseWidth.sce"; ..
                 "Read values from an digital input channel",                  "readDigPort.sce"; ..
                 "Output a single sample to an analog output channel",         "voltUpdate.sce"; ..
                 "Write values from an digital input channel",                 "writeDigPort.sce"];

  subdemolist(:,2) = demopath + subdemolist(:,2);
  
endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
